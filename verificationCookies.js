lesCookies = [{
  id: 1,
  nom: "Chocolate Chip Cookie",
  image: "https://s2.qwant.com/thumbr/0x380/f/a/0211029477f728602f791d45fd5f6ad7210fa045b30c066312f045e9d6b605/Cookie.png?u=http%3A%2F%2Fwww.pngall.com%2Fwp-content%2Fuploads%2F2016%2F07%2FCookie.png&q=0&b=1&p=0&a=0"
}, {
  id: 2,
  nom: "Dark Chocolate Fudge Brownie",
  image: "https://globalassets.starbucks.com/assets/d8097dc1a47b45dda2f7d1f7142cc7b0.jpg?impolicy=1by1_wide_1242"
}, {
  id: 3,
  nom: "Lime-Frosted Coconut Bar",
  image: "https://globalassets.starbucks.com/assets/9bdc63ce47a647d88550e5cc5c357cd6.jpg?impolicy=1by1_wide_1242"
}];

lesCoffee = [{
  id: 1,
  nom: "Caffè Misto",
  image: "https://globalassets.starbucks.com/assets/d668acbc691b47249548a3eeac449916.jpg?impolicy=1by1_wide_1242"
}, {
  id: 2,
  nom: "Caffè Americano",
  image: "https://globalassets.starbucks.com/assets/f12bc8af498d45ed92c5d6f1dac64062.jpg?impolicy=1by1_wide_1242"
}, {
  id: 3,
  nom: "Pistachio Latte",
  image: "https://globalassets.starbucks.com/assets/21fd405326b742b190667e5301e94f68.jpg?impolicy=1by1_wide_1242"
}];

lesIcecream = [{
  id: 1,
  nom: "White Chocolate Crème Frappuccino",
  image: "https://globalassets.starbucks.com/assets/b29b977606704ff9a0238600231faf9c.jpg?impolicy=1by1_wide_1242"
}, {
  id: 2,
  nom: "Strawberry Crème Frappuccino",
  image: "https://globalassets.starbucks.com/assets/d878ff7923b54881a4076de8a0269546.jpg?impolicy=1by1_wide_1242"
}, {
  id: 3,
  nom: "Chocolate Cookie Crumble Crème Frappuccino",
  image: "https://globalassets.starbucks.com/assets/c29e262ae2d6458a976d03733a047b44.jpg?impolicy=1by1_wide_1242"
}];

/**
 * Enregistre le choix du cookie par son id dans le localStorage
 * @param {*} event 
 */
function choixCookies(event) {
  localStorage.setItem("cookieId", event.target.value);
}

/**
 * Enregistre le choix du coffee par son id dans le localStorage
 * @param {*} event 
 */
function choixCoffee(event) {
  localStorage.setItem("coffeeId", event.target.value);
}

/**
 * Enregistre le choix de l'icecream dans le sessionstorage
 * @param {*} event 
 */
function choixIcecream(event) {
  sessionStorage.setItem("icecreamId", event.target.value);
}

/**
 * Trouve l'objet dans la un array a partir de son id
 * @param {*} id id de l'objet
 * @param {*} liste d'objets
 * @returns un objet
 */
function getArticleFrom(id, liste) {
  console.log(id)
  for (const article of liste) {

    if (article.id == id) {
      console.log(article)
      return article;
    }
  }
}

/**
 * Affiche une card avec les infos d'un objet a dans une div
 * @param {*} objet A afficher
 * @param {*} selecteur L'endroit ou l'afficher
 */
function affiche(objet, selecteur) {
  let chaine = `<div class="card" style="width: 18rem;">
  <img class="card-img-top"
    src="${objet.image}"
    alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">${objet.nom}</h5>
    <p class="card-text"> TEXTE </p>
    <a href="${objet.image}" class="btn btn-primary">Go somewhere</a>
  </div>
</div>`
  document.querySelector(selecteur).innerHTML = chaine;
}

/**
 * Verifie le local et session storage au chargement de la page
 * Si on as des id enregistrer on affiche les objets corespondants.
 */
function checkStorage() {
  if (localStorage.getItem("cookieId")) {
    let id = localStorage.getItem("cookieId")
    let cookie = getArticleFrom(id, lesCookies);
    affiche(cookie, "#divCookie");
  }
  if (localStorage.getItem("coffeeId")) {
    let coffee = getArticleFrom(localStorage.getItem("coffeeId"), lesCoffee);
    affiche(coffee, '#divCoffee');
  }
  if (sessionStorage.getItem("icecreamId")) {
    let icecream = getArticleFrom(sessionStorage.getItem("icecreamId"), lesIcecream)
    affiche(icecream, '#divIcecream')
  }
}


document.querySelector("#cookie").addEventListener("change", function (event) {
  choixCookies(event);
  let cookie = getArticleFrom(event.target.value, lesCookies);
  affiche(cookie, "#divCookie")
})

document.querySelector("#coffee").addEventListener("change", function (event) {
  choixCoffee(event);
  let coffee = getArticleFrom(event.target.value, lesCoffee);
  affiche(coffee, "#divCoffee")
})

document.querySelector("#icecream").addEventListener("change", function (event) {
  choixIcecream(event);
  let icecream = getArticleFrom(event.target.value, lesIcecream);
  affiche(icecream, "#divIcecream");
})

checkStorage()